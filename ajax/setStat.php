<?php 

	$id = isset($_POST['id']) ? intval($_POST['id']) : die('Error: restricted access');
	
	if($id == 0) die('Error: restricted access');
	
	//Подключаем основные файлы
	include __DIR__.'/../core.php';
	
	//Записываем статистику
	$STH = DB::PDO()->prepare('INSERT INTO `banners_stat` SET `banner_id` = ?');
	$STH->execute(array($id));
		
	//Пишем в мемкеш очередь 
	$redis->rpush('round', $id);
	
	//Отдаем новый баннер
	$banner = array(); 
	$banner['id'] = $id;
	$banner['banner'] = Functions::getRandomBanner($memcacheD);
	echo json_encode($banner);
	
	