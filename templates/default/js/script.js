document.addEventListener("DOMContentLoaded", function() {
	
	// alert(1);
	
	banner = document.querySelector(".banner");
	count = 0;
	
	function checkBanner(bannerImg){
		let id = bannerImg.getAttribute('data-id');
		
		if(bannerImg.complete){
			console.log('Banner N'+id+' load');
			getNewBanner(id);
		}else{
			bannerImg.addEventListener("load", function(){
				console.log('Banner N'+id+' load');
				getNewBanner(id);
			});
		}
	}
	
	function getNewBanner(id){
		ajax().post('/ajax/setStat.php', { id: id }).then(
			function (response) {
				console.log('Статистика для баннера N'+response.id+' записана');
				setNewBanner(response.banner);
			}
		)
	}
	
	function setNewBanner(data){
		banner.innerHTML = '<img src="/files/banners/'+data.name+'" alt="" data-id="'+data.id+'"/>';
		
		checkBanner(document.querySelector(".banner img"));
		count++;
	}
  
	checkBanner(document.querySelector(".banner img"));
  
	//Считаем сколько проходит за 10 секунд
	setInterval(function(){
		console.clear();
		console.log('За 10 секунд загружено: '+count+' баннеров');
		count = 0;
	}, 10000);
  
});