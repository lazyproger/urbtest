<?php

defined('URB_LOGIC') or die('Error: restricted access');

include __DIR__.'/classes/db.php';

//MEMCACHED
$memcacheD = new Memcached;
$memcacheD->addServer('localhost', 11211); 

//MYSQL (PDO)
new DB;

//REDIS
$redis = new Redis;
$redis->connect('127.0.0.1', 6379);