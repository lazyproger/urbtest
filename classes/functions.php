<?php
	
	/* Класс с часто используемыми функциями*/
	
	class Functions {

	
		public static function getRandomBanner($memcacheD){
			
			//Получаем случайный баннер
			$banners_count = $memcacheD->get('banners_count'); //Кол.во баннеров
			$banner = $memcacheD->get('banner_'.rand(1, $banners_count).'');
			
			return $banner;
		}
	
	
	}