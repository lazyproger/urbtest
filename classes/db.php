<?php

defined('URB_LOGIC') or die('Error: restricted access');

class DB
{
	private static $instance = NULL;

	public static function PDO()
    {
        if (is_null(static::$instance)) {
            try {
                static::$instance = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_BASE, DB_USER, DB_PASSWORD,
                    array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
                        PDO::MYSQL_ATTR_USE_BUFFERED_QUERY
                    )
                );
            } catch (PDOException $e) {
                die('<p>DB Error: ' . $e->getMessage() . '</p>');
            }
        }

        return static::$instance;
    }
}
