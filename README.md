# README #

Некоторые системные файлы важные для запуска

1. /cron/bannerInMemcached.php
скрипт из базы закидывает баннеры в мемкеш

2. /cron/checkRound.php
скрипт берет очередь из редиса и отправляет в /cron/magic.php
Считает времся выполение magic.php 
Пушит результат в `banners_log`

Структура `banners_log`

CREATE TABLE IF NOT EXISTS `banners_log` (
`id_query` int(11) NOT NULL,
  `code` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `banners_log`
 ADD PRIMARY KEY (`id_query`);
 
ALTER TABLE `banners_log`
 MODIFY `id_query` int(11) NOT NULL AUTO_INCREMENT;