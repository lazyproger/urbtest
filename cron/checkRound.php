<?php
	include __DIR__.'/../core.php';

	$round = $redis->lrange('round', 0, -1);
	if(count($round) > 0){
		$round = json_encode($round);
		$redis->del('round');
		$time = array();
		
		$time[0] = microtime(true);
		
		$ch = curl_init('http://urb.lazyproger.ru/cron/magic.php');                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $round);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($round))                                                                       
		);             

		$result = curl_exec($ch);
		
		$time[1] = microtime(true);
		$currentTime = $time[1] - $time[0];
		
		//Если запрос вписывается в 15 секунд, пишем в лог 1
		if($currentTime <= 15){
			$STH = DB::PDO()->prepare('INSERT INTO `banners_log` SET `code` = ?');
			$STH->execute(array(1));
			
		}else{
			//Иначе 0
			$STH = DB::PDO()->prepare('INSERT INTO `banners_log` SET `code` = ?');
			$STH->execute(array(0));
		}
	}