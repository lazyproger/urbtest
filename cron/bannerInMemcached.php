<?php
	//Подключаем основные файлы
	include __DIR__.'/../core.php';

	$banners = DB::PDO()->query("SELECT * FROM `banners`")->fetchAll();
	
	$i=0;
	foreach($banners as $banner){
		$i++;
		$memcacheD->set('banner_'.$i.'', array('name' => $banner['banner_name'], 'id' => $banner['id_banner']));
	}
	
	//Записываем баннеры в мемкеш
	$memcacheD->set('banners_count', $i);